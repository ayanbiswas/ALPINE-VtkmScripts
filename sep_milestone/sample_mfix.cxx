//============================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//  Copyright 2014 National Technology & Engineering Solutions of Sandia, LLC (NTESS).
//  Copyright 2014 UT-Battelle, LLC.
//  Copyright 2018 Los Alamos National Security.
//
//  Under the terms of Contract DE-NA0003525 with NTESS,
//  the U.S. Government retains certain rights in this software.
//
//  Under the terms of Contract DE-AC52-06NA25396 with Los Alamos National
//  Laboratory (LANL), the U.S. Government retains certain rights in
//  this software.
//============================================================================
#include <vtkm/cont/ArrayHandleConstant.h>
#include <vtkm/cont/DeviceAdapterAlgorithm.h>
#include <vtkm/worklet/DispatcherMapField.h>
#include <vtkm/worklet/WorkletMapField.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>

// specify the regular sampling window
const int samp_offset=10; 

template <typename Device>
class StratifiedParticleSampler
{
public:
  StratifiedParticleSampler(const vtkm::Vec<vtkm::FloatDefault, 3>& _min,
    const vtkm::Vec<vtkm::FloatDefault, 3>& _max,
    const vtkm::Vec<vtkm::Id, 3>& _dims)
    : Min(_min)
    , Dims(_dims)
    , Dxdydz((_max - Min) / Dims)
  {}

  class BinParticlesWorklet : public vtkm::worklet::WorkletMapField
  {
  public:
    using ControlSignature = void(FieldIn<> coord, FieldOut<> label);

    using ExecutionSignature = void(_1, _2);

    VTKM_CONT
    BinParticlesWorklet(vtkm::Vec<vtkm::FloatDefault, 3> _min,
                        vtkm::Vec<vtkm::FloatDefault, 3> _max,
                        vtkm::Vec<vtkm::Id, 3> _dims)
    : Min(_min)
    , Dims(_dims)
    , Dxdydz((_max - Min) / Dims)
    {
    }

    template <typename CoordVecType, typename IdType>
    VTKM_EXEC void operator()(const CoordVecType& coord, IdType& label) const
    {
      vtkm::Vec<vtkm::Id, 3> ijk = (coord - Min) / Dxdydz;
      label = ijk[0] + ijk[1] * Dims[0] + ijk[2] * Dims[0] * Dims[1];
    }

  private:
    vtkm::Vec<vtkm::FloatDefault, 3> Min;
    vtkm::Vec<vtkm::Id, 3> Dims;
    vtkm::Vec<vtkm::FloatDefault, 3> Dxdydz;
  };

  class RegularSamplingWorklet : public vtkm::worklet::WorkletMapField
  {
  public:
    using ControlSignature = void(FieldIn<> offset, FieldOut<> flag);
    using ExecutionSignature = void(_1, _2);

    RegularSamplingWorklet(vtkm::Id _rate) : rate(_rate) {}

    VTKM_EXEC
    void operator()(const vtkm::Id offset, vtkm::Id& flag) const
    {
        flag = (offset % rate == 0);
    }

  private:
    vtkm::Id rate;
  };

  template <typename CoordType>
  vtkm::cont::DataSet Execute(const CoordType& particles)
  {
    using Algorithm = vtkm::cont::DeviceAdapterAlgorithm<Device>;

    // make a copy of the coordinates of the input particles
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> coords;
    Algorithm::Copy(particles, coords);

    // bin particles into a uniform grid and give each particle a cell id
    vtkm::cont::ArrayHandle<vtkm::Id> cellIds;

    BinParticlesWorklet cellIdWorklet(this->Min, this->Max, this->Dims);
    vtkm::worklet::DispatcherMapField<BinParticlesWorklet, Device> dispatchCellId(cellIdWorklet);
    dispatchCellId.Invoke(coords, cellIds);

    // sort/group particles according to their cell id.
    Algorithm::SortByKey(cellIds, coords);

    // calculate offsets for particles within each cell
    vtkm::cont::ArrayHandleConstant<vtkm::Id> ones(1, cellIds.GetNumberOfValues());
    vtkm::cont::ArrayHandle<vtkm::Id> offsets;
    Algorithm::ScanInclusiveByKey(cellIds, ones, offsets);

    // regular sampling on particles within each bin, take particles with
    // offset that is a multiple of sampling rate.
    vtkm::cont::ArrayHandle<vtkm::Id> flags;
    RegularSamplingWorklet samplingWorklet(samp_offset);
    vtkm::worklet::DispatcherMapField<RegularSamplingWorklet, Device> dispatchSampling(samplingWorklet);
    dispatchSampling.Invoke(offsets, flags);

    // copy particles sampled
    vtkm::cont::ArrayHandle<vtkm::Vec<vtkm::FloatDefault, 3>> samples;
    Algorithm::CopyIf(coords, flags, samples);

    for (int i = 0; i < samples.GetNumberOfValues(); i++) {
      std::cout << samples.GetPortalConstControl().Get(i) << std::endl;
    }

    vtkm::cont::DataSet result;
    result.AddCoordinateSystem(vtkm::cont::CoordinateSystem("coordinates", samples));

    return result;
  }

private:
  vtkm::Vec<vtkm::FloatDefault, 3> Min;
  vtkm::Vec<vtkm::FloatDefault, 3> Max;
  vtkm::Vec<vtkm::Id, 3> Dims;
  vtkm::Vec<vtkm::FloatDefault, 3> Dxdydz;
};


int main(int argc, char* argv[])
{
  vtkm::io::reader::VTKDataSetReader reader(argv[1]);
  vtkm::cont::DataSet input = reader.ReadDataSet();

  auto coords = input.GetCoordinateSystem(0).GetData();

  StratifiedParticleSampler<vtkm::cont::DeviceAdapterTagSerial> sampler(
    {0.0f, 0.0f, 0.0f}, {0.01f, 0.01f, 0.01f}, {10, 10, 10});

  auto sample = sampler.Execute(coords);

  std::string filename = "sampled_hist_regular.vtk";
  vtkm::io::writer::VTKDataSetWriter writer(filename.c_str());
  writer.WriteDataSet(sample);
}