This code demonstrates histogram-based regular sampling on a particle data set. MFIX-Exa simulation data set was tested for verification and correctness of the algorithm.

It takes a *.vtk (legacy) file in and outputs a histogram-based regular smapled vtk data set out. 
However, currently, the vtk-m reader does not recognize some of the information present in the input vtk data set. So, to run this code, some modification of the vtk data is needed. 

An example test case: Create the input vtk data using ParaView-5.5.0. Save in ascii mode and open the *.vtk file in a text editor. Search for METADATA and delete the section (generally 4-5 lines). There are typically two places that has METADAT tags and have to be deleted. Now, the first time when METADATA tag is found, after deleting the associated 4-5 lines, add a new line with the text "CELLS 0 0". Save the file and now use this file as the input and the code should work.

In this example, the regular sampling is done on a 3D histogram with 10 bins on each dimension. Also, every 10th data point is stored while sampling.

To run the code: ./mfix_SERIAL input_data.vtk
