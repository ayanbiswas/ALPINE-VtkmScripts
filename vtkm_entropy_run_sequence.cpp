//
// Created by Ayan Biswas on 4/26/18.
//

#include <vtkm/filter/Threshold.h>
#include <vtkm/cont/testing/MakeTestDataSet.h>
#include <vtkm/io/reader/VTKDataSetReader.h>
#include <vtkm/io/reader/VTKStructuredGridReader.h>
#include <vtkm/filter/MarchingCubes.h>

#include <vtkm/Math.h>
#include <vtkm/cont/ArrayHandleUniformPointCoordinates.h>
#include <vtkm/cont/CellSetSingleType.h>
#include <vtkm/cont/DataSet.h>
#include <vtkm/cont/DataSetBuilderUniform.h>
#include <vtkm/cont/DataSetFieldAdd.h>
#include <vtkm/cont/DynamicArrayHandle.h>
//include <vtkm/cont/testing/Testing.h>
#include <vtkm/filter/CleanGrid.h>
#include <vtkm/io/writer/VTKDataSetWriter.h>
#include <vtkm/worklet/FieldHistogram.h>
#include <vtkm/filter/Entropy.h>
#include <vtkm/filter/Entropy.h>

#include <string>
#include <iostream>
#include <fstream>

void PrintHistogram(vtkm::cont::ArrayHandle<vtkm::Id> bins,
                    vtkm::Id numberOfBins,
                    const vtkm::Range& range,
                    vtkm::Float32 delta)
{
  vtkm::cont::ArrayHandle<vtkm::Id>::PortalConstControl binPortal = bins.GetPortalConstControl();

  vtkm::Id sum = 0;
  vtkm::Float64 entropy = 0;
  for (vtkm::Id i = 0; i < numberOfBins; i++)
  {
    vtkm::Float64 lo = range.Min + (static_cast<vtkm::Float64>(i) * delta);
    vtkm::Float64 hi = lo + delta;
    sum += binPortal.Get(i);
    std::cout << "  BIN[" << i << "] Range[" << lo << ", " << hi << "] = " << binPortal.Get(i)
              << std::endl;
  }
  // calculate entropy
  for (vtkm::Id i = 0; i < numberOfBins; i++)
  {
    vtkm::Float64 cur_val =  binPortal.Get(i)/(1.0*sum);
    if(cur_val > 0)
    entropy -= cur_val*log2(cur_val);
  }
  std::cout<<"entropy = "<<entropy<<std::endl;

}

int main(int argc, char* argv[]) {

  // Check the number of parameters
  if (argc < 3) 
  {
	std::cout<<"Usage: ./vtkm_entropy_run_sequence  <start> <end>"<<std::endl;
	return 1;
  }

  int start_id = atoi(argv[1]); 
  int end_id = atoi(argv[2]);
  std::string file_name1 = "/home/biswas/asteroid_VO2_vtk_data/ayan_files/yA31_v02_300x300x300_"; 
  std::string file_name2 = ".vtk" ;
  int number = start_id;
  int nbins = 10;

  // write entropy values to the output files
  std::ofstream myfile;
  myfile.open ("../entropy_vals.txt");
  //myfile << "This is the first cell in the first column.\n";

 
  for (int i=start_id; i<end_id; i++)
  {  
	number = i;
	std::string filename = file_name1 + static_cast<std::ostringstream*>( &(std::ostringstream() << number) )->str()+ file_name2;
	//std::string filename = file_name1 + Result + file_name2;
	std::cout<<"Processing file "<<filename<<std::endl;

	// read a vtk dataset
	vtkm::io::reader::VTKDataSetReader reader(filename.c_str());
	vtkm::cont::DataSet dataSet = reader.ReadDataSet();
	std::cout<<"The data is read in"<<std::endl;

	// Try to get the entropy 
	vtkm::filter::Entropy entropyFilter;

	///// calculate entropy of "nodevar" field of the data set /////
	entropyFilter.SetNumberOfBins(nbins); //set number of bins
	entropyFilter.SetActiveField("RTData");
	vtkm::cont::DataSet resultEntropy = entropyFilter.Execute(dataSet);

	///// get entropy from resultEntropy /////
	vtkm::cont::ArrayHandle<vtkm::Float64> entropy;
	resultEntropy.GetField("entropy").GetData().CopyTo(entropy);
	vtkm::cont::ArrayHandle<vtkm::Float64>::PortalConstControl portal =
	entropy.GetPortalConstControl();
	vtkm::Float64 entropyFromFilter = portal.Get(0);

	std::cout<<"Entropy is "<<entropyFromFilter<<std::endl;
	myfile <<entropyFromFilter<< "\n";

  }
  myfile.close();
    
    return 0;
 
}
